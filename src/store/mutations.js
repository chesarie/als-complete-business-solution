// https://vuex.vuejs.org/en/mutations.html
import Vue from 'vue'
export default {
  //
  SET_USERS: (state, payload) => {
    state.users = payload
    state.technicians = state.users.filter(user => user.role === 'Technician')
  },
  SET_TASK_STATUS: (state, payload) => {
    state.taskStatus = payload
  },
  SET_TECHNICIANS: (state) => {
    state.technicians = state.users.filter(user => user.role === 'Technician')
    // console.log(state.technicians )
  },
  SET_CUSTOMERS: (state, payload) => {
    state.customers = payload
    // console.log(state.customers )
  },
  ADD_TASK: (state, payload) => {
    var date = new Date()
    var timestamp = date.getTime()
    let taskLength = state.servicesTask.length
    let lastId = state.servicesTask[taskLength - 1].id
    let newtask = {}
    newtask.id = lastId + 1
    newtask.customerId = payload.customerId
    newtask.statusID = payload.statusID
    newtask.onCreateTimeStamp = timestamp
    newtask.machine = payload.machine
    newtask.issue = payload.machine.issue
    // console.log(newtask)
    state.servicesTask.push(newtask)
    // console.log(state.servicesTask)
  },
  LIST_TASK: (state, payload) => {
    state.servicesTask = payload
  },
  SET_SELECTED_SO_LOGS: (state, payload) => {
    state.selectedServiceOrderLogs = payload
    // console.log(state.selectedServiceOrderLogs)
  },
  SET_SELECTED_SO_ACTIONS: (state, payload) => {
    state.selectedServiceOrderActions = payload
    // console.log(state.selectedServiceOrderLogs)
  },
  SET_SELECTED_SO: (state, payload) => {
    state.selectedServiceOrder = payload
    // console.log( state.selectedServiceOrder )
  },
  SET_SELECTED_SO_COMMENTS: (state, payload) => {
    state.selectedServiceOrderComments = payload
    // console.log(state.selectedServiceOrderComments)
  },
  SET_SELECTED_SO_PURCHASE: (state, payload) => {
    state.selectedServiceOrderPurchase = payload
    // console.log(state.selectedServiceOrderComments)
  }

}
