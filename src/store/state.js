// https://vuex.vuejs.org/en/state.html

export default {
  //
  users: [], // all users
  technicians: [],
  customers: [], // all so_customers
  viewTask: false, // so edit/viewtask full screen modal
  newFormTask: false, // so form modal
  snackbar: false, // so snackbar
  snackbarcolor: 'success',
  alertMessage: '',
  selectedSO: '',
  editServiceOrder: false,
  servicesTask: [ ], // all service order
  selectedServiceOrderPurchase: [],
  selectedServiceOrder: {},
  selectedServiceOrderLogs: {},
  selectedServiceOrderComments: {},
  selectedServiceOrderActions: {},
  taskStatus: []
}
