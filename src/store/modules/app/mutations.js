import { set, toggle } from '@/utils/vuex'

export default {
  setDrawer: set('drawer'),
  setImage: set('image'),
  setColor: set('color'),
  setLoggedUser: set('loggeduser'),
  setNavigationMenu: set('navigationMenu'),
  setPaymentTerms: set('PaymentTerms'),
  setPaymentStatus: set('PaymentStatus'),
  setAccessButtons: set('accessButtons'),
  setTransferButtons: set('accessButtons'),
  toggleDrawer: toggle('drawer')
}
