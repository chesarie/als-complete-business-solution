// https://vuex.vuejs.org/en/getters.html

export default {
  //
  getServiceTasks: (state) => (id) => {
    return state.servicesTask.filter(task => task.status_id === id)
  },
  getServiceOrder: (state) => (id) => {
    return state.servicesTask.filter(task => task.id === id)
  },
  getAllStatus: (state) => {
    return state.taskStatus
  },
  getStatus: (state) => (id) => {
    return state.taskStatus.filter(status => status.id == id)
  },
  getCustomer: (state) => (id) => {
    return state.customers.filter(customer => customer.id == id)
  },
  getUser: (state) => (id) => {
    return state.users.filter(user => user.id === id)
  }

}
