// https://vuex.vuejs.org/en/actions.html
import Vue from 'vue'

export default {
  //

  GET_USERS: ({ commit }) => {
    Vue.prototype.$http.post('user/list').then(response => {
      // console.log("users")
      response.data.data != null ? commit('SET_USERS', response.data.data) : console.log('null')
    }).catch(e => {
      console.log(e)
    })
  },
  GET_TASK: ({ commit, getters }, assigneeId) => {
    // console.log(Vue.prototype.$session.get('role'))
    var role = Vue.prototype.$session.get('role')
    // console.log(assignee_id)

    if (role === 'Technician') {
      var entry = { 'assignees': assigneeId }
      Vue.prototype.$http.post('service_order_task_per_user/get', entry).then(response => {
        // console.log('get task response')
        // console.log(response)
        response.data.data != null ? commit('LIST_TASK', response.data.data) : console.log('null')
      }).catch(e => {
        console.log(e)
      })
    } else {
      Vue.prototype.$http.post('service_order/list').then(response => {
        // console.log('get task response')
     //   console.log(response)
        response.data.data != null ? commit('LIST_TASK', response.data.data) : console.log('null')
      }).catch(e => {
        console.log(e)
      })
    }
  },
  GET_CUSTOMERS: ({ commit }) => {
    Vue.prototype.$http.post('service_order_customer/list').then(response => {
      response.data.data != null ? commit('SET_CUSTOMERS', response.data.data) : console.log('null')
    }).catch(e => {
      console.log(e)
    })
  },
  GET_SELECTED_SO_LOGS: ({ commit }, soId) => {
    var req = { so_id: soId }
    Vue.prototype.$http.post('service_order_log/get', req).then(response => {
      response.data.data != null ? commit('SET_SELECTED_SO_LOGS', response.data.data) : console.log('null')
    }).catch(e => {
      console.log(e)
    })
  },
  GET_SELECTED_SERVICE_ORDER_DATA: ({ commit, getters, dispatch }, soId) => {
    // console.log("action dispatch GET_SELECTED_SERVICE_ORDER_DATA")

    dispatch('GET_TASK')
    let SO = getters.getServiceOrder(soId)
    commit('SET_SELECTED_SO', SO)
    var req = {}
    req.so_id = soId
    Vue.prototype.$http.post('service_order_log/get', req).then(response => {
      response.data.data != null ? commit('SET_SELECTED_SO_LOGS', response.data.data) : console.log('null')
    }).catch(e => {
      console.log(e)
    })
    dispatch('GET_SELECTED_SO_COMMENTS', soId)
    dispatch('GET_SELECTED_SO_PURCHASE', soId)
  },

  GET_SELECTED_SO_COMMENTS: ({ commit }, soId) => {
    var req = { so_id: soId }
    Vue.prototype.$http.post('service_order_comment/get', req).then(response => {
      // console.log(response)
      if (response.data.status) {
        response.data.data != null ? commit('SET_SELECTED_SO_COMMENTS', response.data.data) : console.log('null')

        //   this.$store.dispatch('GET_SELECTED_SO_LOGS', updateTask.id)
      }
    }).catch(e => {
      console.log(e)
    })
  },
  GET_SELECTED_SO_PURCHASE: ({ commit }, soId) => {
    var req = { so_id: soId }
    Vue.prototype.$http.post('service_order_purchase/get', req).then(response => {
      // console.log(response)
      if (response.data.status) {
        response.data.data != null ? commit('SET_SELECTED_SO_PURCHASE', response.data.data) : console.log('null')
        //   this.$store.dispatch('GET_SELECTED_SO_LOGS', updateTask.id)
      }
    }).catch(e => {
      console.log(e)
    })
  },
  SERVICE_TRACKER_GET: ({ commit }, soId) => {
    var req = { id: parseInt(soId) }
    // console.log(req)
    Vue.prototype.$http.post('service_order/get', req).then(response => {
    //  console.log(response)
      if (response.data.status) {
        // response.data.data != null ? commit('SET_SELECTED_SO_PURCHASE', response.data.data): console.log("null")
        //   this.$store.dispatch('GET_SELECTED_SO_LOGS', updateTask.id)
      }
    }).catch(e => {
      console.log(e)
    })
  },
  GET_STATUS_LIST: ({ commit }) => {
    Vue.prototype.$http.post('service_settings/list').then(response => {
      if (response.data.status) {
      //  console.log('get settings')
        var statuses = response.data.data.filter(setting => setting.name === 'statuses')[0]
        let formattedData = statuses.data.map(status => {
          return JSON.parse(status)
        })
        commit('SET_TASK_STATUS', formattedData)
      } else {

      }
    }).catch(e => {
      console.log(e)
    })
  }
}
