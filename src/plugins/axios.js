import Vue from 'vue'
// Lib imports
import axios from 'axios'
var SERVER = 'http://localhost:9001'
var production = false
if (production) {
  SERVER = 'http://changethis.com:9001'
}
Vue.prototype.$http = axios.create({
  baseURL: SERVER + '/api/'
})

Vue.prototype.$fileserver = SERVER + '/public/'
