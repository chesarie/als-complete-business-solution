import Vue from 'vue'

Vue.prototype.$allowedAction = function (action) {
  let arr = this.$session.get('permissions').action.split(',')
  return arr.includes(action)
}
Vue.prototype.$allowedActionStatus = function (action, status) {
  if (action != status) {
    return true
  }
  return false
}
Vue.prototype.$allowedActionButton = function (button, status, permissiondata) {
  if (permissiondata) {
    for (var i = 0; i < permissiondata.length; i++) {
      if (status == 'action-only') {
        if (button == permissiondata[i].button) {
          return true
        }
      }
      if (permissiondata[i].status == status && button == permissiondata[i].button) {
        return true
      }
    }
  }
  return false
}
Vue.prototype.$money = function (amount) {
  if (!amount) { amount = 0 }
  if (isNaN(amount)) {
    amount = 0
  }
  amount = parseFloat(amount).toFixed(2)
  return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}

Vue.prototype.$IsNum = function (input) {
  if (isNaN(parseFloat(input)) || input == null) {
    return 0
  }
  input = parseFloat(input).toFixed(2)
  return parseFloat(input)
}
Vue.prototype.$printThis = function () {
  window.print()
}
Vue.prototype.$groupBy = key => array =>
  array.reduce((objectsByKeyValue, obj) => {
    const value = obj[key]
    objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj)
    return objectsByKeyValue
  }, {})

Vue.prototype.$statusColor = function (status) {
  switch (status) {
    case 'Available':
      return 'success'
    case 'Sold':
      return 'warning'
    case 'On Process':
      return 'info'
    case 'Delivered':
      return 'success'
    case 'Cancelled':
      return '#ddd'
    default:
      return 'info'
  }
}
