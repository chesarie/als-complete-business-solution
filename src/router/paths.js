/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */
export default [
  {
    path: '/dashboard',
    // Relative to /src/views
    view: 'Dashboard'
  },
  {
    path: '/user-profile',
    name: 'User Profile',
    view: 'UserProfile'
  },
  {
    path: '/login',
    name: 'Login',
    view: 'Login'
  },
  {
    path: '/users',
    name: 'Users Accounts',
    view: 'accounts/Users'
  },
  {
    path: '/suppliers',
    name: 'Manage Suppliers',
    view: 'accounts/Suppliers'
  },
  {
    path: '/rate-discounts',
    name: 'Manage Discounts',
    view: 'accounts/Discounts'
  },
  {
    path: '/branches',
    name: 'Branches',
    view: 'accounts/Branches'
  },
  {
    path: '/customers',
    name: 'Customers',
    view: 'accounts/Customers'
  },
  {
    path: '/settings',
    name: 'General Settings',
    view: 'accounts/Settings'
  },
  {
    path: '/product-settings',
    name: 'Product Settings',
    view: 'accounts/ProductSettings'
  },
  {
    path: '/table-list',
    name: 'Table List',
    view: 'TableList'
  },
  {
    path: '/purchase',
    name: 'Manage P.O.',
    view: 'purchase/Purchase'
  },
  {
    path: '/consignment',
    name: 'Consignments',
    view: 'purchase/Consignment'
  },
  {
    path: '/consignment/:id',
    name: 'View Consignment',
    view: 'purchase/ViewConsignment'
  },
  {
    path: '/purchase/:id',
    name: 'View Purchase',
    view: 'purchase/ViewPurchase'
  },
  {
    path: '/purchase-monitor/:id',
    name: 'Monitor Purchase',
    view: 'purchase/MonitorPurchase'
  },
  {
    path: '/credit-memo',
    name: 'Credit Memo',
    view: 'purchase/CreditMemo'
  },
  {
    path: '/receive-items',
    name: 'Receive Items',
    view: 'purchase/Receive'
  },
  {
    path: '/account-payables',
    name: 'Account Payables',
    view: 'purchase/Payables'
  },
  {
    path: '/product-inventory',
    name: 'Product Inventory',
    view: 'inventory/Products'
  },
  {
    path: '/product/:id',
    name: 'Product Items',
    view: 'inventory/ProductItems'
  },
  {
    path: '/stock-transfer',
    name: 'Stocks Transfer',
    view: 'inventory/Transfer'
  },
  {
    path: '/transfer/:id',
    name: 'View Transfer',
    view: 'inventory/ViewTransfer'
  },
  {
    path: '/assembly',
    name: 'Assembly Request',
    view: 'inventory/Assembly'
  },
  {
    path: '/assembler/:id',
    name: 'Assembler',
    view: 'inventory/Assembler'
  },
  {
    path: '/assemble-item',
    name: 'Assemble Item',
    view: 'services/AssembleItem'
  },
  {
    path: '/assembledview/:id',
    name: 'Assemble View',
    view: 'inventory/AssembleView'
  },
  {
    path: '/item-tracker',
    name: 'Item Tracker',
    view: 'inventory/ItemTracker'
  },
  {
    path: '/sales-order',
    name: 'Sales Orders',
    view: 'sales/SalesOrder'
  },
  {
    path: '/order/:id',
    name: 'View Order',
    view: 'sales/ViewOrder'
  },
  {
    path: '/pricelist',
    name: 'Price List',
    view: 'sales/PriceList'
  },
  {
    path: '/quotations',
    name: 'Quotations',
    view: 'sales/Quotations'
  },
  {
    path: '/typography',
    view: 'Typography'
  },
  {
    path: '/icons',
    view: 'Icons'
  },
  {
    path: '/maps',
    view: 'Maps'
  },
  {
    path: '/notifications',
    view: 'Notifications'
  },
  {
    path: '/upgrade',
    name: 'Upgrade to PRO',
    view: 'Upgrade'
  },
  {
    path: '/services-order',
    name: 'Service Order',
    view: 'services/Task'
  },
  {
    path: '/services-dashboard',
    name: 'Service Management',
    view: 'services/Dashboard'
  },
  {
    path: '/services-purchase',
    name: 'Service Purchase',
    view: 'services/Purchase'
  },
  {
    path: '/services-product',
    name: 'Service Product',
    view: 'services/Product'
  },
  {
    path: '/services-tracker',
    name: 'Service Tracker',
    view: 'services/Tracker'
  },
  {
    path: '/services-tariff',
    name: 'Service Tariff',
    view: 'services/Tariff'
  }
]
