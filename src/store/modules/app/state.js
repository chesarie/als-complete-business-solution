export default {
  drawer: false,
  accessButtons: [],
  transferButtons: [],
  color: 'main',
  image: '../img/alfalink.png',
  loggeduser: null,
  navigationMenu: [
    {
      group: 'Purchase',
      icon: 'fas fa-file-invoice',
      links: [
        {
          to: '/purchase',
          icon: 'fas fa-file-alt',
          page: 'Manage P.O.'
        },
        {
          to: '/consignment',
          icon: 'fas fa-file-import',
          page: 'Consignments'
        },
        {
          to: '/credit-memo',
          icon: 'far fa-edit',
          page: 'Credit Memo'
        },
        {
          to: '/account-payables',
          icon: 'fas fa-file-invoice',
          page: 'Account Payables'
        },
        {
          to: '/receive-items',
          icon: 'fas fa-truck-loading',
          page: 'Receiving'
        }
      ]
    },
    {
      group: 'Inventory',
      icon: 'fas fa-dolly-flatbed',
      links: [
        {
          to: '/product-inventory',
          icon: 'fas fa-boxes',
          page: 'Products Management'
        },
        {
          to: '/stock-transfer',
          icon: 'fas fa-truck-moving',
          page: 'Stock Transfer'
        },
        {
          to: '/assembly',
          icon: 'fas fa-tools',
          page: 'Assembly Request'
        },
        {
          to: '/user-profile',
          icon: 'fas fa-sliders-h',
          page: 'Adjustment'
        },
        {
          to: '/item-tracker',
          icon: 'mdi-database-search',
          page: 'Item Tracker'
        },
        {
          to: '/user-profile',
          icon: 'fas fa-file-alt',
          page: 'Reports'
        }
      ]
    },
    {
      group: 'Sales',
      icon: 'fas fa-cash-register',
      links: [
        {
          to: '/sales-order',
          icon: 'fas fa-store',
          page: 'Sales Order'
        },
        {
          to: '/price-list',
          icon: 'fas fa-tags',
          page: 'Price List'
        },
        {
          to: '/receivable',
          icon: 'fas fa-file-invoice',
          page: 'Account Receivables'
        },
        {
          to: '/quotations',
          icon: 'fas fa-file-alt',
          page: 'Quotations'
        },
        {
          to: '/sales-return',
          icon: 'fas fa-exchange-alt',
          page: 'Returns'
        },
        {
          to: '/sales-reports',
          icon: 'fas fa-chart-bar',
          page: 'Reports'
        }
      ]
    },
    {
      group: 'Services',
      icon: 'fas fa-toolbox',
      links: [
        {
          to: '/services-order',
          icon: 'fas fa-boxes',
          page: 'Service Order'
        },
        {
          to: '/services-purchase',
          icon: 'fas fa-shopping-cart',
          page: 'Service Purchase'
        },
        {
          to: '/services-product',
          icon: 'fas fa-clipboard-list',
          page: 'Service Product'
        },
        {
          to: '/services-tracker',
          icon: 'fas fa-search',
          page: 'Service Tracker'
        },
        {
          to: '/services-dashboard',
          icon: 'fas fa-sliders-h',
          page: 'Service Management'
        },
        {
          to: '/services-tariff',
          icon: 'fas fa-sliders-h',
          page: 'Service Tariff'
        },
        {
          to: '/services-dashboard',
          icon: 'fas fa-chart-area',
          page: 'Reports'
        },
        {
          to: '/assemble-item',
          icon: 'mdi-toolbox',
          page: 'Assembly'
        }
      ]
    },
    {
      group: 'Account And Settings',
      icon: 'fas fa-cogs',
      links: [
        {
          to: '/users',
          icon: 'fas fa-users-cog',
          page: 'User Accounts'
        },
        {
          to: '/product-settings',
          icon: 'fas fa-layer-group',
          page: 'Product Settings'
        },
        {
          to: '/customers',
          icon: 'fas fa-address-book',
          page: 'Customers'
        },
        {
          to: '/suppliers',
          icon: 'fas fa-address-card',
          page: 'Suppliers'
        },
        {
          to: '/branches',
          icon: 'fas fa-store-alt',
          page: 'Branches'
        },
        {
          to: '/rate-discounts',
          icon: 'fas fa-percent',
          page: 'Rates & Discounts'
        },
        {
          to: '/settings',
          icon: 'fas fa-cog',
          page: 'General Settings'
        }
      ]
    }
  ],
  sidebarBackgroundColor: 'rgba(27, 27, 27, 0.74)',
  PaymentTerms: ['CBD', 'COD', '15 Days', '30 Days', '45 Days', '60 Days', '90 Days', 'Others'],
  PaymentStatus: ['On Process', 'On Hold', 'Cheque Ready', 'Cheque Issued', 'Paid'],
  PurchaseStatuses: ['On Process', 'Posted', 'Verified P.R.',
    'Recommended P.R.', 'Approved P.R.', 'Recommended for P.O.', 'Approved for P.O.',
    'Issued', 'Lacking Delivery', 'Partial Delivery', 'Delivered'
  ],
  TransferStatuses: ['On Process', 'Posted', 'Approved', 'Released', 'Received']
}
